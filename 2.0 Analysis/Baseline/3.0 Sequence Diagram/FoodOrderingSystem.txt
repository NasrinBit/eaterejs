@startuml
Autonumber
	
actor Customer
boundary SavedOrder
control RestaurantSupervisor
entity RestaurantEmployee
entity OrderStatus
entity DeliveryPerson
database CustomerOrder
collections CusineList
collections RestaurantList
collections Menuitems


Customer-> CusineList: SelectCusine(Indian,Chinese,Japanese,Thai,Vegetarian)
CusineList -> RestaurantList : Select Restaurant (ResID, ResName, PhoneNumber)
RestaurantList -> Menuitems : Menu list
Menuitems -> Customer : Display Menu items (Breakfast Menu, Lunch Menu, Dinner Menu)
Customer -> SavedOrder : Select Items,Qty (List, Qty, Cost, Tax, Total)
SavedOrder -> RestaurantSupervisor : Order info to Restaurant (Customer Details, Order Details)

alt Order Approved

	RestaurantSupervisor -> RestaurantEmployee : Order Preparation (Customer Details, Order Details)
	RestaurantSupervisor -> Customer : Order Confirmation Message (Order status, Expected time)
	RestaurantEmployee -> OrderStatus : Update Status (Ready, Delayed)
	OrderStatus -> DeliveryPerson : Order Ready to deliver (Location, Customer Details, Charges)
	DeliveryPerson -> Customer : OrderDelivery (Receives Payment, Update status)
	DeliveryPerson -> OrderStatus : UpdateStatus (Update Status as Delivered)
end

group Order Rejected by RestaurantSupervisor 
else Order Rejected
	RestaurantSupervisor -> Customer : Order Rejection Message (Reason)

title Food Ordering System

end
@enduml