myApp.post("/start",function (req, res) {
  res.render('start');
});
myApp.get("/restaurantLogin", auth, function (req, res) {
  
  res.render('/restaurantLogin');
});
myApp.get("/restaurantRegister", auth, function (req, res) {
  
  res.render('restaurantRegister');
});
myApp.get("/restaurantrUpdateInfo", auth, function (req, res) {
  
  res.render('restaurantrUpdateInfo');
});

myApp.get("/restaurantrViewOrders", auth, function (req, res) {
  
  res.render('restaurantrViewOrders');
});
myApp.get("/restaurantrUpdateOrderStatus", auth, function (req, res) {
  
  res.render('restaurantrUpdateOrderStatus');
});
myApp.get("/restaurantrViewMenu", auth, function (req, res) {
  
  res.render('restaurantrViewMenu');
});
myApp.get("/restaurantrAddMenuItem", auth, function (req, res) {
  
  res.render('restaurantrAddMenuItem');
});
myApp.get("/restaurantEditMenuItem", auth, function (req, res) {
  
  res.render('restaurantEditMenuItem');
});
myApp.get("/customerLogin", auth, function (req, res) {
  
  res.render('/customerLogin');
});
myApp.get("/customerRegister", auth, function (req, res) {
  
  res.render('customerRegister');
});
myApp.get("/customerUpdateInfo", auth, function (req, res) {
  
  res.render('customerUpdateInfo');
});
myApp.get("/customerCreateOrder", auth, function (req, res) {
  
  res.render('customerCreateOrder');
});
myApp.get("/customerViewOrder", auth, function (req, res) {
  
  res.render('customerViewOrder');
});
myApp.get("/customerUpdateOrder", auth, function (req, res) {
  
  res.render('customerUpdateOrder');
});

myApp.get("/deliveryPersonLogin", auth, function (req, res) {
  
  res.render('deliveryPersonLogin');
});
myApp.get("/deliveryPersonRegister", auth, function (req, res) {
  
  res.render('deliveryPersonRegister');
});
myApp.get("/deliveryPersonUpdateInfo", auth, function (req, res) {
  
  res.render('deliveryPersonUpdateInfo');
});
myApp.get("/deliveryPersonViewOrders", auth, function (req, res) {
  
  res.render('deliveryPersonViewOrders');
});
myApp.get("/deliveryPersonCreateDeliveryRequest", auth, function (req, res) {
  
  res.render('deliveryPersonCreateDeliveryRequest');
});
myApp.get("/deliveryPersonUpdateDeliveryStatus", auth, function (req, res) {
  
  res.render('deliveryPersonUpdateDeliveryStatus');
});

