DELETE FROM Cuisine 
  WHERE ID = ?;
DELETE FROM Customer 
  WHERE ID = ?;
DELETE FROM [Delivery Person] 
  WHERE ID = ?;
DELETE FROM [Delivery Request] 
  WHERE ID = ?;
DELETE FROM DeliveryOption 
  WHERE ID = ?;
DELETE FROM MenuCategory 
  WHERE ID = ?;
DELETE FROM MenuDeals 
  WHERE ID = ?;
DELETE FROM MenuItem 
  WHERE ID = ?;
DELETE FROM MenuType 
  WHERE ID = ?;
DELETE FROM [Order] 
  WHERE ID = ?;
DELETE FROM OrderItems 
  WHERE ID = ?;
DELETE FROM OrderStatus 
  WHERE ID = ?;
DELETE FROM Person 
  WHERE ID = ?;
DELETE FROM PersonType 
  WHERE ID = ?;
DELETE FROM Restaurant 
  WHERE ID = ?;

