INSERT INTO Cuisine
  (ID, 
  Name) 
VALUES 
  (?, 
  ?);
INSERT INTO Customer
  (ID) 
VALUES 
  (?);
INSERT INTO [Delivery Person]
  (ID, 
  DriverVehicleNumber) 
VALUES 
  (?, 
  ?);
INSERT INTO [Delivery Request]
  (ID, 
  OrderID, 
  PickupTime, 
  DeliveryTime, 
  [Delivery PersonID]) 
VALUES 
  (?, 
  ?, 
  ?, 
  ?, 
  ?);
INSERT INTO DeliveryOption
  (ID, 
  Name) 
VALUES 
  (?, 
  ?);
INSERT INTO MenuCategory
  (ID, 
  Name) 
VALUES 
  (?, 
  ?);
INSERT INTO MenuDeals
  (ID, 
  Name) 
VALUES 
  (?, 
  ?);
INSERT INTO MenuItem
  (ID, 
  MenuCategoryID, 
  MenuTypeID, 
  MenuDealsID, 
  Name, 
  Description, 
  Price, 
  Available, 
  RestaurantID) 
VALUES 
  (?, 
  ?, 
  ?, 
  ?, 
  ?, 
  ?, 
  ?, 
  ?, 
  ?);
INSERT INTO MenuType
  (ID, 
  Name) 
VALUES 
  (?, 
  ?);
INSERT INTO [Order]
  (ID, 
  Number, 
  [Date], 
  Time, 
  Total, 
  Tax, 
  Notes, 
  PickupTime, 
  DeliveryTime, 
  DeliveryOptionID, 
  OrderStatusID, 
  CustomerID, 
  [Delivery PersonID], 
  RestaurantID) 
VALUES 
  (?, 
  ?, 
  ?, 
  ?, 
  ?, 
  ?, 
  ?, 
  ?, 
  ?, 
  ?, 
  ?, 
  ?, 
  ?, 
  ?);
INSERT INTO OrderItems
  (ID, 
  Quantity, 
  OrderID, 
  MenuItemID) 
VALUES 
  (?, 
  ?, 
  ?, 
  ?);
INSERT INTO OrderStatus
  (ID, 
  Name) 
VALUES 
  (?, 
  ?);
INSERT INTO Person
  (ID, 
  PersonTypeID, 
  Name, 
  Address, 
  Telephone, 
  Email, 
  Password, 
  Confirmpassword, 
  Description, 
  Discriminator, 
  RestaurantID, 
  CustomerID, 
  [Delivery PersonID]) 
VALUES 
  (?, 
  ?, 
  ?, 
  ?, 
  ?, 
  ?, 
  ?, 
  ?, 
  ?, 
  ?, 
  ?, 
  ?, 
  ?);
INSERT INTO PersonType
  (ID, 
  Name) 
VALUES 
  (?, 
  ?);
INSERT INTO Restaurant
  (CuisineID, 
  ID) 
VALUES 
  (?, 
  ?);

