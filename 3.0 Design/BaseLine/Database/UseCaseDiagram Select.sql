SELECT ID, Name 
  FROM Cuisine;
SELECT ID 
  FROM Customer;
SELECT ID, DriverVehicleNumber 
  FROM [Delivery Person];
SELECT ID, OrderID, PickupTime, DeliveryTime, [Delivery PersonID] 
  FROM [Delivery Request];
SELECT ID, Name 
  FROM DeliveryOption;
SELECT ID, Name 
  FROM MenuCategory;
SELECT ID, Name 
  FROM MenuDeals;
SELECT ID, MenuCategoryID, MenuTypeID, MenuDealsID, Name, Description, Price, Available, RestaurantID 
  FROM MenuItem;
SELECT ID, Name 
  FROM MenuType;
SELECT ID, Number, [Date], Time, Total, Tax, Notes, PickupTime, DeliveryTime, DeliveryOptionID, OrderStatusID, CustomerID, [Delivery PersonID], RestaurantID 
  FROM [Order];
SELECT ID, Quantity, OrderID, MenuItemID 
  FROM OrderItems;
SELECT ID, Name 
  FROM OrderStatus;
SELECT ID, PersonTypeID, Name, Address, Telephone, Email, Password, Confirmpassword, Description, Discriminator, RestaurantID, CustomerID, [Delivery PersonID] 
  FROM Person;
SELECT ID, Name 
  FROM PersonType;
SELECT CuisineID, ID 
  FROM Restaurant;

