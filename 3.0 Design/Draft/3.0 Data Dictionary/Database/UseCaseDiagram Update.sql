UPDATE Cuisine SET 
  Name = ? 
WHERE
  ID = ?;
UPDATE Customer SET 
   
WHERE
  ID = ?;
UPDATE [Delivery Person] SET 
  DriverVehicleNumber = ? 
WHERE
  ID = ?;
UPDATE [Delivery Request] SET 
  OrderID = ?, 
  PickupTime = ?, 
  DeliveryTime = ?, 
  [Delivery PersonID] = ? 
WHERE
  ID = ?;
UPDATE DeliveryOption SET 
  Name = ? 
WHERE
  ID = ?;
UPDATE MenuCategory SET 
  Name = ? 
WHERE
  ID = ?;
UPDATE MenuDeals SET 
  Name = ? 
WHERE
  ID = ?;
UPDATE MenuItem SET 
  MenuCategoryID = ?, 
  MenuTypeID = ?, 
  MenuDealsID = ?, 
  Name = ?, 
  Description = ?, 
  Price = ?, 
  Available = ?, 
  RestaurantID = ? 
WHERE
  ID = ?;
UPDATE MenuType SET 
  Name = ? 
WHERE
  ID = ?;
UPDATE [Order] SET 
  Number = ?, 
  [Date] = ?, 
  Time = ?, 
  Total = ?, 
  Tax = ?, 
  Notes = ?, 
  PickupTime = ?, 
  DeliveryTime = ?, 
  DeliveryOptionID = ?, 
  OrderStatusID = ?, 
  CustomerID = ?, 
  [Delivery PersonID] = ?, 
  RestaurantID = ? 
WHERE
  ID = ?;
UPDATE OrderItems SET 
  Quantity = ?, 
  OrderID = ?, 
  MenuItemID = ? 
WHERE
  ID = ?;
UPDATE OrderStatus SET 
  Name = ? 
WHERE
  ID = ?;
UPDATE Person SET 
  PersonTypeID = ?, 
  Name = ?, 
  Address = ?, 
  Telephone = ?, 
  Email = ?, 
  Password = ?, 
  Confirmpassword = ?, 
  Description = ?, 
  Discriminator = ?, 
  RestaurantID = ?, 
  CustomerID = ?, 
  [Delivery PersonID] = ? 
WHERE
  ID = ?;
UPDATE PersonType SET 
  Name = ? 
WHERE
  ID = ?;
UPDATE Restaurant SET 
  CuisineID = ? 
WHERE
  ID = ?;

