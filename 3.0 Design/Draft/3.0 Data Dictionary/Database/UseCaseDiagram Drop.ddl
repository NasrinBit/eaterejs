ALTER TABLE MenuItem DROP CONSTRAINT FKMenuItem722856;
ALTER TABLE [Order] DROP CONSTRAINT FKOrder143628;
ALTER TABLE [Order] DROP CONSTRAINT FKOrder556711;
ALTER TABLE [Order] DROP CONSTRAINT FKOrder343354;
ALTER TABLE [Order] DROP CONSTRAINT FKOrder26752;
ALTER TABLE [Order] DROP CONSTRAINT FKOrder582424;
ALTER TABLE [Delivery Request] DROP CONSTRAINT [FKDelivery R518310];
ALTER TABLE [Delivery Request] DROP CONSTRAINT [FKDelivery R762806];
ALTER TABLE Restaurant DROP CONSTRAINT FKRestaurant635760;
ALTER TABLE Person DROP CONSTRAINT FKPerson323219;
ALTER TABLE Person DROP CONSTRAINT FKPerson748305;
ALTER TABLE Person DROP CONSTRAINT FKPerson161389;
ALTER TABLE Person DROP CONSTRAINT FKPerson596543;
ALTER TABLE MenuItem DROP CONSTRAINT FKMenuItem670707;
ALTER TABLE MenuItem DROP CONSTRAINT FKMenuItem843297;
ALTER TABLE MenuItem DROP CONSTRAINT FKMenuItem88053;
ALTER TABLE OrderItems DROP CONSTRAINT OrderItems;
ALTER TABLE OrderItems DROP CONSTRAINT OrderItems;
DROP TABLE Cuisine;
DROP TABLE Customer;
DROP TABLE [Delivery Person];
DROP TABLE [Delivery Request];
DROP TABLE DeliveryOption;
DROP TABLE MenuCategory;
DROP TABLE MenuDeals;
DROP TABLE MenuItem;
DROP TABLE MenuType;
DROP TABLE [Order];
DROP TABLE OrderItems;
DROP TABLE OrderStatus;
DROP TABLE Person;
DROP TABLE PersonType;
DROP TABLE Restaurant;

