CREATE TABLE Cuisine (
  ID   int IDENTITY NOT NULL, 
  Name varchar(50) NULL, 
  PRIMARY KEY (ID));
CREATE TABLE Customer (
  ID int IDENTITY NOT NULL, 
  PRIMARY KEY (ID));
CREATE TABLE [Delivery Person] (
  ID                  int IDENTITY NOT NULL, 
  DriverVehicleNumber varchar(20) NULL, 
  PRIMARY KEY (ID));
CREATE TABLE [Delivery Request] (
  ID                  int IDENTITY NOT NULL, 
  OrderID             int NOT NULL, 
  PickupTime          datetime NULL, 
  DeliveryTime        datetime NULL, 
  [Delivery PersonID] int NOT NULL, 
  PRIMARY KEY (ID));
CREATE TABLE DeliveryOption (
  ID   char(20) NOT NULL, 
  Name varchar(20) NULL UNIQUE, 
  PRIMARY KEY (ID));
CREATE TABLE MenuCategory (
  ID   int IDENTITY NOT NULL, 
  Name varchar(100) NULL, 
  PRIMARY KEY (ID));
CREATE TABLE MenuDeals (
  ID   int IDENTITY NOT NULL, 
  Name varchar(100) NULL, 
  PRIMARY KEY (ID));
CREATE TABLE MenuItem (
  ID             int IDENTITY NOT NULL, 
  MenuCategoryID int NULL, 
  MenuTypeID     int NULL, 
  MenuDealsID    int NULL, 
  Name           varchar(50) NULL, 
  Description    varchar(255) NULL, 
  Price          numeric(10, 2) NULL, 
  Available      bit NOT NULL, 
  RestaurantID   int NOT NULL, 
  PRIMARY KEY (ID));
CREATE TABLE MenuType (
  ID   int IDENTITY NOT NULL, 
  Name varchar(100) NULL, 
  PRIMARY KEY (ID));
CREATE TABLE [Order] (
  ID                  int IDENTITY NOT NULL, 
  Number              int NOT NULL, 
  [Date]              datetime NULL, 
  Time                datetime NULL, 
  Total               numeric(10, 2) NULL, 
  Tax                 numeric(10, 2) NULL, 
  Notes               varchar(255) NULL, 
  PickupTime          datetime NULL, 
  DeliveryTime        datetime NULL, 
  DeliveryOptionID    char(20) NOT NULL, 
  OrderStatusID       char(10) NOT NULL, 
  CustomerID          int NOT NULL, 
  [Delivery PersonID] int NULL, 
  RestaurantID        int NOT NULL, 
  PRIMARY KEY (ID));
CREATE TABLE OrderItems (
  ID         int IDENTITY NOT NULL, 
  Quantity   int NOT NULL, 
  OrderID    int NOT NULL, 
  MenuItemID int NOT NULL, 
  PRIMARY KEY (ID));
CREATE TABLE OrderStatus (
  ID   char(10) DEFAULT 'NEW' NOT NULL, 
  Name varchar(20) NULL UNIQUE, 
  PRIMARY KEY (ID));
CREATE TABLE Person (
  ID                  int IDENTITY NOT NULL, 
  PersonTypeID        char(20) NOT NULL, 
  Name                varchar(50) NULL, 
  Address             varchar(255) NULL, 
  Telephone           varchar(15) NULL, 
  Email               varchar(255) NULL, 
  Password            varchar(10) NULL, 
  Confirmpassword     varchar(10) NULL, 
  Description         varchar(255) NULL, 
  Discriminator       varchar(255) NOT NULL, 
  RestaurantID        int NULL UNIQUE, 
  CustomerID          int NULL UNIQUE, 
  [Delivery PersonID] int NULL UNIQUE, 
  PRIMARY KEY (ID));
CREATE TABLE PersonType (
  ID   char(20) NOT NULL, 
  Name varchar(20) NULL UNIQUE, 
  PRIMARY KEY (ID));
CREATE TABLE Restaurant (
  CuisineID int NOT NULL, 
  ID        int IDENTITY NOT NULL, 
  PRIMARY KEY (ID));
ALTER TABLE MenuItem ADD CONSTRAINT FKMenuItem_Restaurant FOREIGN KEY (RestaurantID) REFERENCES Restaurant (ID);
ALTER TABLE [Order] ADD CONSTRAINT FKOrder_DeliveryPerson FOREIGN KEY ([Delivery PersonID]) REFERENCES [Delivery Person] (ID);
ALTER TABLE [Order] ADD CONSTRAINT FKOrder_Customer FOREIGN KEY (CustomerID) REFERENCES Customer (ID);
ALTER TABLE [Order] ADD CONSTRAINT FKOrder_Restaurant FOREIGN KEY (RestaurantID) REFERENCES Restaurant (ID);
ALTER TABLE [Order] ADD CONSTRAINT FKOrder_DeliveryOption FOREIGN KEY (DeliveryOptionID) REFERENCES DeliveryOption (ID);
ALTER TABLE [Order] ADD CONSTRAINT FKOrder_OrderStatus FOREIGN KEY (OrderStatusID) REFERENCES OrderStatus (ID);
ALTER TABLE [Delivery Request] ADD CONSTRAINT [FKDeliveryRequest_DeliveryPerson] FOREIGN KEY ([DeliveryPersonID]) REFERENCES [Delivery Person] (ID);
ALTER TABLE [Delivery Request] ADD CONSTRAINT [FKDeliveryRequest_Order] FOREIGN KEY (OrderID) REFERENCES [Order] (ID);
ALTER TABLE Restaurant ADD CONSTRAINT FKRestaurant_Cuisine FOREIGN KEY (CuisineID) REFERENCES Cuisine (ID);
ALTER TABLE Person ADD CONSTRAINT FKPerson_Restaurant FOREIGN KEY (RestaurantID) REFERENCES Restaurant (ID);
ALTER TABLE Person ADD CONSTRAINT FKPerson_Customer FOREIGN KEY (CustomerID) REFERENCES Customer (ID);
ALTER TABLE Person ADD CONSTRAINT FKPerson_DeliveryPerson FOREIGN KEY ([Delivery PersonID]) REFERENCES [Delivery Person] (ID);
ALTER TABLE Person ADD CONSTRAINT FKPerson_PersonType FOREIGN KEY (PersonTypeID) REFERENCES PersonType (ID);
ALTER TABLE MenuItem ADD CONSTRAINT FKMenuItem_MenuCategory FOREIGN KEY (MenuCategoryID) REFERENCES MenuCategory (ID);
ALTER TABLE MenuItem ADD CONSTRAINT FKMenuItem_MenuType FOREIGN KEY (MenuTypeID) REFERENCES MenuType (ID);
ALTER TABLE MenuItem ADD CONSTRAINT FKMenuItem_MenuDeals FOREIGN KEY (MenuDealsID) REFERENCES MenuDeals (ID);
ALTER TABLE OrderItems ADD CONSTRAINT OrderItems_Order FOREIGN KEY (OrderID) REFERENCES [Order] (ID);
ALTER TABLE OrderItems ADD CONSTRAINT OrderItems_Menu FOREIGN KEY (MenuItemID) REFERENCES MenuItem (ID);

