function validationFunction()
{
  document.getElementById("NameError").style.visibility="hidden";
  document.getElementById("PriceError").style.visibility="hidden";
  document.getElementById("AvailableError").style.visibility="hidden";   
  var inputName=document.getElementById("inputName").value;
  var inputPrice=document.getElementById("inputPrice").value;
  var inputAvailable=document.getElementById("inputAvailable").value;
  var valid=true;
  if(inputName=="" )
  {
   document.getElementById("NameError").style.visibility="visible";
   valid=false;
  }
 if(inputPrice=="")
  {
   document.getElementById("PriceError").style.visibility="visible";
   valid=false;
  }
  if(isNaN(inputPrice))
  {
   document.getElementById("PriceError").style.visibility="visible";
   valid=false;
  }
  if(inputAvailable=="Choose")
  {
   document.getElementById("AvailableError").style.visibility="visible";
   valid=false;
  }
 return valid;
}
function resetFunction()
{
 document.getElementById("NameError").style.visibility="hidden";
 document.getElementById("PriceError").style.visibility="hidden";
 document.getElementById("AvailableError").style.visibility="hidden";  
 return true;
}
