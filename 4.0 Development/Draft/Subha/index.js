/*Import Dependencies */
var express = require("express");
var path = require("path");
const bodyParser = require('body-parser')
/* Setup Express Validator */
const { check, validationResult } = require('express-validator');
/*Setup Database Connection*/
const mongoose = require("mongoose");
var session = require('express-session');
mongoose.connect("mongodb://localhost:27017/Eater", {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});
// create a new express app
var myApp = express();
myApp.use(session({
  secret: 'secret',
  resave: false,
  saveUninitialized: true,
}));
/* set up variables to use packages*/
myApp.use(bodyParser.urlencoded({ extended: false }))
/* use public folder for CSS etc.*/
myApp.use(express.static(path.join(__dirname, 'public')));
/* set path to public folders and view folders*/
myApp.set('views', path.join(__dirname, 'views'));
/* set the view engine to use EJS*/
myApp.set('view engine', 'ejs');
/*Create Model for Restaurant*/
const Restaurant = mongoose.model("RegisterForm",
  {
    restaurantName: String,
    address: String,
    telephone: String,
    email: String,
    password: String,
    confirmPassword: String,
    description: String,
    cuisine: String,
  });
/*Create Model for Customer*/
const Customer = mongoose.model("RegisterFormCustomer",
  {
    customerName: String,
    address: String,
    telephone: String,
    email: String,
    password: String,
    confirmPassword: String,
    description: String,
  });
/*Create Model for Delivery Person*/
const DeliveryPerson = mongoose.model("RegisterFormDeliveryPerson",
  {
    deliveryPersonName: String,
    address: String,
    telephone: String,
    email: String,
    password: String,
    confirmPassword: String,
    description: String,
    vehicleNumber: String,
  });
/* Create Model for Food Menu*/
const AddMenuItem = mongoose.model("AddMenuItem", {
  restaurantName: String,
  Item: String,
  Description: String,
  Price: String,
  Available: String,
});
/*Create auth Function */
var auth = function (req, res, next) {
  if (!req.session.restaurantName) {
    res.render("index")
  }
  else {
    next();
  }
}
/* set up different routes (pages) of the website
 home page should show the Login form page*/
myApp.get("/", function (req, res) {
  req.session.formName = "Login Page";
  res.render('index');
});
/* Submit Login Form */
myApp.post("/", function (req, res) {
  var user = req.body.email;
  var pass = req.body.password;
  if (user && pass) {
    Restaurant.findOne({ email: user, password: pass }).exec(function (err, restaurant) {
      if (restaurant) {
        req.session.restaurantName = restaurant.restaurantName;
        req.session.restaurantInfo = restaurant;
        res.redirect("Orders");
        return;
      }
    });
  }
  if (user && pass) {
    Customer.findOne({ email: user, password: pass }).exec(function (err, customer) {
      if (customer) {
        req.session.customerName = customer.restaurantName;
        req.session.customerInfo = customer;
        res.redirect("OrdersCustomer"); return;
      }
    });
  }
  if (user && pass) {
    DeliveryPerson.findOne({ email: user, password: pass }).exec(function (err, deliveryPerson) {
      if (deliveryPerson) {
        req.session.deliveryPersonName = deliveryPerson.restaurantName;
        req.session.deliveryPersonInfo = deliveryPerson;
        res.redirect("OrdersDeliveryPerson");
      }
      else {
        res.render("index", { message: "Invalid Email/Password" });
      }
    });
  }
})
/* Show About Page */
myApp.get("/about", function (req, res) {
  req.session.formName = 'About';
  res.render('about', { loggedInUser: req.session.restaurantName, formName: req.session.formName });
});
/*Show Add New Item Page */
myApp.get("/AddNewItem", auth, function (req, res) {
  req.session.formName = "Add New Item";
  res.render('AddNewItem', { loggedInUser: req.session.restaurantName, formName: req.session.formName });
});
/* Submit Add New Item Page */
myApp.post("/AddNewItem", [
  check('inputName', '**This is a required field').not().isEmpty(),
  check('inputPrice', '**Enter in ($$$.$$) fromat').matches(/^([0-9]{1,3})((\.){0,1})([0-9]{0,2})$/)
], function (req, res) {
  const errors = validationResult(req)
  var info = {};

  if (!errors.isEmpty()) {
    console.log(info)
    req.session.formName = "Add New Item";
    res.render('AddNewItem', { errors: errors.array(), loggedInUser: req.session.restaurantName, formName: req.session.formName });
  }
  else {
    var restaurantNameForm = req.session.restaurantName;
    var itemForm = req.body.inputName;
    var priceForm = req.body.inputPrice;
    var descriptionForm = req.body.inputDescription;
    var availableForm = req.body.inputAvailable;
    var menuData =
    {
      restaurantName: restaurantNameForm,
      Item: itemForm,
      Description: descriptionForm,
      Price: priceForm,
      Available: availableForm,
    };
    var myMenu = new AddMenuItem(menuData);

    myMenu.save().then(function () {
      // wait for async operation to complete and then render the dahboard
      console.log("New Item added");
      res.redirect("menu");
    });
  }
});
/* Show Menu Page */
myApp.get("/menu", auth, function (req, res) {
  AddMenuItem.find({ restaurantName: req.session.restaurantName }).exec(function (err, menuData) {
    req.session.formName = "Menu";
    if (menuData.length > 0) {
      res.render('menu', { items: menuData, loggedInUser: req.session.restaurantName, formName: req.session.formName });
    }
    else {
      res.render('menu', { loggedInUser: req.session.restaurantName, formName: req.session.formName });
    }
  })
});
/* Show Orders Page */
myApp.get("/Orders", auth, function (req, res) {
  req.session.formName = "Orders";
  res.render('Orders', { loggedInUser: req.session.restaurantName, formName: req.session.formName });
});
/* Show Orders Page for Customer*/
myApp.get("/OrdersCustomer", function (req, res) {
  req.session.formName = "OrdersCustomer";
  res.render('OrdersCustomer', { loggedInUser: req.session.customerName, formName: req.session.formName });
});
/* Show Orders Page for Delivery Person*/
myApp.get("/OrdersDeliveryPerson", function (req, res) {
  req.session.formName = "OrdersDeliveryPerson";
  res.render('OrdersDeliveryPerson', { loggedInUser: req.session.deliveryPersonName, formName: req.session.formName });
});
/* Show Register Page */
myApp.get("/RegisterForm", function (req, res) {
  res.render('RegisterForm', { loggedInUser: 'New User' });
});
/* Submit Register Page */
myApp.post("/RegisterForm", [
  check('restaurantName', '**This is a required field').not().isEmpty(),
  check('address', '**This is a required field').not().isEmpty(),
  check('telephone', '**Enter valid format(XXX-XXX-XXXX)').matches(/^([0-9]{3})([\.-]{1})([0-9]{3})([\.-]{1})([0-9]{4})$/),
  check('email', '**Enter valid E-mail address').matches(/^([a-z 0-9\.-]+)@([a-z 0-9-]+).([a-z]{2,8})(.[a-z]{2,8})?$/),
  check('password', '**Enter atleast 8 characters').isLength({ min: 8 }),
  check('confirmPassword', '**Password does not match').custom((value, { req }) => value === req.body.password)
], function (req, res) {
  const errors = validationResult(req)
  var info = {};

  if (!errors.isEmpty()) {
    res.render('RegisterForm', { errors: errors.array(), loggedInUser: 'New User' });
  }
  else {
    var restaurantNameForm = req.body.restaurantName;
    var addressForm = req.body.address;
    var telephoneForm = req.body.telephone;
    var emailForm = req.body.email;
    var passwordForm = req.body.password;
    var confirmPasswordForm = req.body.confirmPassword;
    var descriptionForm = req.body.description;
    var cuisineForm = req.body.cuisine;
    var restaurantData =
    {
      restaurantName: restaurantNameForm,
      address: addressForm,
      telephone: telephoneForm,
      email: emailForm,
      password: passwordForm,
      confirmPassword: confirmPasswordForm,
      description: descriptionForm,
      cuisine: cuisineForm,
    };
    var myRestaurant = new Restaurant(restaurantData);

    myRestaurant.save().then(function () {
      // wait for async operation to complete and then render the dahboard
      console.log("New restaurant added");
      req.session.restaurantName = restaurantData.restaurantName;
      req.session.formName = "Orders";
      res.render("Orders", { loggedInUser: req.session.restaurantName, formName: req.session.formName });
    });
  }
});
/* Show Register Page for Customer */
myApp.get("/RegisterFormCustomer", function (req, res) {
  res.render('RegisterFormCustomer', { loggedInUser: 'New User' });
});
/* Submit Register Page for Customer*/
myApp.post("/RegisterFormCustomer", [
  check('customerName', '**This is a required field').not().isEmpty(),
  check('address', '**This is a required field').not().isEmpty(),
  check('telephone', '**Enter valid format(XXX-XXX-XXXX)').matches(/^([0-9]{3})([\.-]{1})([0-9]{3})([\.-]{1})([0-9]{4})$/),
  check('email', '**Enter valid E-mail address').matches(/^([a-z 0-9\.-]+)@([a-z 0-9-]+).([a-z]{2,8})(.[a-z]{2,8})?$/),
  check('password', '**Enter atleast 8 characters').isLength({ min: 8 }),
  check('confirmPassword', '**Password does not match').custom((value, { req }) => value === req.body.password)
], function (req, res) {
  const errors = validationResult(req)
  var info = {};

  if (!errors.isEmpty()) {
    res.render('RegisterFormCustomer', { errors: errors.array(), loggedInUser: 'New User' });
  }
  else {
    var customerNameForm = req.body.customerName;
    var addressForm = req.body.address;
    var telephoneForm = req.body.telephone;
    var emailForm = req.body.email;
    var passwordForm = req.body.password;
    var confirmPasswordForm = req.body.confirmPassword;
    var descriptionForm = req.body.description;
    var customerData =
    {
      customerName: customerNameForm,
      address: addressForm,
      telephone: telephoneForm,
      email: emailForm,
      password: passwordForm,
      confirmPassword: confirmPasswordForm,
      description: descriptionForm,
    };
    var myCustomer = new Customer(customerData);

    myCustomer.save().then(function () {
      // wait for async operation to complete and then render the dahboard
      console.log("New Customer added");
      req.session.customerName = customerData.customerName;
      req.session.formName = "OrdersCustomer";
      res.render("OrdersCustomer", { loggedInUser: req.session.customerName, formName: req.session.formName });
    });
  }
});
/* Show Register Page for Delivery Person */
myApp.get("/RegisterFormDeliveryPerson", function (req, res) {
  res.render('RegisterFormDeliveryPerson', { loggedInUser: 'New User' });
});
/* Submit Register Page for Delivery Person*/
myApp.post("/RegisterFormDeliveryPerson", [
  check('deliveryPersonName', '**This is a required field').not().isEmpty(),
  check('address', '**This is a required field').not().isEmpty(),
  check('telephone', '**Enter valid format(XXX-XXX-XXXX)').matches(/^([0-9]{3})([\.-]{1})([0-9]{3})([\.-]{1})([0-9]{4})$/),
  check('email', '**Enter valid E-mail address').matches(/^([a-z 0-9\.-]+)@([a-z 0-9-]+).([a-z]{2,8})(.[a-z]{2,8})?$/),
  check('password', '**Enter atleast 8 characters').isLength({ min: 8 }),
  check('confirmPassword', '**Password does not match').custom((value, { req }) => value === req.body.password)
], function (req, res) {
  const errors = validationResult(req)
  var info = {};

  if (!errors.isEmpty()) {
    res.render('RegisterFormDeliveryPerson', { errors: errors.array(), loggedInUser: 'New User' });
  }
  else {
    var deliveryPersonNameForm = req.body.deliveryPersonName;
    var addressForm = req.body.address;
    var telephoneForm = req.body.telephone;
    var emailForm = req.body.email;
    var passwordForm = req.body.password;
    var confirmPasswordForm = req.body.confirmPassword;
    var descriptionForm = req.body.description;
    var vehicleNumberForm = req.body.vehicleNumber;
    var deliveryPersonData =
    {
      deliveryPersonName: deliveryPersonNameForm,
      address: addressForm,
      telephone: telephoneForm,
      email: emailForm,
      password: passwordForm,
      confirmPassword: confirmPasswordForm,
      description: descriptionForm,
      vehicleNumber: vehicleNumberForm,
    };
    var mydeliveryPerson = new DeliveryPerson(deliveryPersonData);

    mydeliveryPerson.save().then(function () {
      // wait for async operation to complete and then render the dahboard
      console.log("New Delivery Person added");
      req.session.deliveryPersonName = deliveryPersonData.deliveryPersonName;
      req.session.formName = "OrdersDeliveryPerson";
      res.render("OrdersDeliveryPerson", { loggedInUser: req.session.deliveryPersonName, formName: req.session.formName });
    });
  }
});
/* Submit Update Item Page */
myApp.post("/UpdateItem", auth, [
  check('inputName', '**This is a required field').not().isEmpty(),
  check('inputPrice', '**Enter in ($$$.$$) fromat').matches(/^([0-9]{1,3})((\.){0,1})([0-9]{0,2})$/)
], function (req, res) {

  const errors = validationResult(req)
  var info = {};

  if (!errors.isEmpty()) {
    console.log(info)
    res.render('AddNewItem', { errors: errors.array(), loggedInUser: req.session.restaurantName });
  }
  else {
    AddMenuItem.updateOne({ restaurantName: req.session.restaurantName, Item: req.body.inputName }, {
      Item: req.body.inputName,
      Description: req.body.inputDescription
      , Price: req.body.inputPrice
      , Available: req.body.inputAvailable
    }).exec(function (err, docs) {
      console.log(docs);
      res.redirect("menu");
    })
  }
});
/* Show Update Item Page */
myApp.get("/edit", auth, function (req, res) {
  AddMenuItem.findById({ _id: req.query.id }, function (err, item) {
    req.session.formName = "Update Menu Item";
    res.render('UpdateItem', { loggedInUser: req.session.restaurantName, UpdateMenu: item, formName: req.session.formName });
  });
})
/* Show Update Information Page */
myApp.get("/UpdateInfo", auth, function (req, res) {
  req.session.formName = "Update Restaurant Information";
  res.render('UpdateInfo', { loggedInUser: req.session.restaurantName, formName: req.session.formName, restaurantInfo1: req.session.restaurantInfo });
})
/*Show Update Page */
myApp.post("/UpdateInfo", auth, function (req, res) {
  Restaurant.updateOne({ restaurantName: req.body.restaurantName }, {
    address: req.body.address
    , telephone: req.body.telephone
    , email: req.body.email
    , password: req.body.password
    , confirmPassword: req.body.confirmPassword
    , description: req.body.description
    , cuisine: req.body.cuisine
  }).exec(function (err, docs) {
    console.log("Updated");
    res.redirect("menu");
  })
});
/*Show Delete Page */
myApp.get("/delete", auth, function (req, res) {
  AddMenuItem.findByIdAndDelete({ _id: req.query.id }).exec(function (err, item) {
    console.log("Deleted");
    res.redirect("menu");
  })
})
/* Show DashBoardHeader*/
myApp.get("/dashboardheader", function (req, res) {
  res.render('dashboardheader', { loggedInUser: req.session.restaurantName, formName: req.session.formName });
});
/* Show DashBoardHeader for Customer*/
myApp.get("/dashboardHeaderCustomer", function (req, res) {
  res.render('dashboardheaderCustomer', { loggedInUser: req.session.customerName, formName: req.session.formName });
});
/* Show DashBoardHeader for Delivery Person*/
myApp.get("/dashboardHeaderDeliveryPerson", function (req, res) {
  res.render('dashboardHeaderDeliveryPerson', { loggedInUser: req.session.deliveryPersonName, formName: req.session.formName });
});
/*Show header Page */
myApp.get("/header", function (req, res) {
  res.render('header', { loggedInUser: req.session.restaurantName });
});
/* Show Create order for Customer Page*/
myApp.get("/CreateOrderCustomer", function (req, res) {
  res.render('CreateOrderCustomer', { loggedInUser: req.session.customerName, formName: req.session.formName });
});
/* Show  Update Information for Customer Page*/
myApp.get("/UpdateInfoCustomer", function (req, res) {
  req.session.formName = "Update Customer Information";
  res.render('UpdateInfoCustomer', { loggedInUser: req.session.customerName, formName: req.session.formName, customerInfo1: req.session.customerInfo });
})
/*Show Update Page */
myApp.post("/UpdateInfoCustomer", function (req, res) {
  Customer.updateOne({ customerName: req.body.customerName }, {
    address: req.body.address
    , telephone: req.body.telephone
    , email: req.body.email
    , password: req.body.password
    , confirmPassword: req.body.confirmPassword
    , description: req.body.description
  }).exec(function (err, docs) {
    console.log("Updated");
    res.redirect("OrdersCustomer");
  })
});
/* Show  Update Information for Delivery Person Page*/
myApp.get("/UpdateInfoDeliveryPerson", function (req, res) {
  req.session.formName = "Update Delivery Person Information";
  res.render('UpdateInfoDeliveryPerson', { loggedInUser: req.session.deliveryPersonName, formName: req.session.formName, deliveryPersonInfo1: req.session.deliveryPersonInfo });
})
/*Show Update Page */
myApp.post("/UpdateInfoDeliveryPerson", function (req, res) {
  DeliveryPerson.updateOne({ deliveryPersonName: req.body.deliveryPersonName }, {
    address: req.body.address
    , telephone: req.body.telephone
    , email: req.body.email
    , password: req.body.password
    , confirmPassword: req.body.confirmPassword
    , description: req.body.description
    , vehicleNumber: req.body.vehicleNumber
  }).exec(function (err, docs) {
    console.log("Updated");
    res.redirect("OrdersDeliveryPerson");
  })
});
/*Show Logout Page */
myApp.get("/LogOut", auth, function (req, res) {
  req.session.destroy();
  res.redirect("/");
});
/* start the server and listen at a port */
myApp.listen(8082);
/* Show Message if  server is started and connected to port*/
console.log("My website is running on the port 8082");
