function validationFunction()
{
    document.getElementById("emailError").style.visibility="hidden";
    document.getElementById("passwordError").style.visibility="hidden";
    var email=document.getElementById('email').value;   
    var password=document.getElementById('password').value;
    var valid=true;
    var emailRegex=/^([a-z 0-9\.-]+)@([a-z 0-9-]+).([a-z]{2,8})(.[a-z]{2,8})?$/;
    if(!(emailRegex.test(email)))
    {
        document.getElementById("emailError").style.visibility="visible";
        valid=false;
    }
    if(password.length<8)
    {
        document.getElementById("passwordError").style.visibility="visible";
        valid=false;
    }   
    return valid;
}
    