function validationFunction()
{
    document.getElementById("restaurantNameError").style.visibility="hidden";
    document.getElementById("addressError").style.visibility="hidden";
    document.getElementById("telephoneError").style.visibility="hidden";
    document.getElementById("emailError").style.visibility="hidden";
    document.getElementById("passwordError").style.visibility="hidden";
    document.getElementById("confirmPasswordError").style.visibility="hidden";   
    var restaurantName=document.getElementById('restaurantName').value;
    var address=document.getElementById('address').value;
    var telephone=document.getElementById('telephone').value;
    var email=document.getElementById('email').value;   
    var password=document.getElementById('password').value;
    var confirmPassword=document.getElementById('confirmPassword').value;
    var valid =true;  
    var telephoneRegex=/^([0-9]{3})([\.-]{1})([0-9]{3})([\.-]{1})([0-9]{4})$/;
    var emailRegex=/^([a-z 0-9\.-]+)@([a-z 0-9-]+).([a-z]{2,8})(.[a-z]{2,8})?$/;
    if(restaurantName=="")
    {
        document.getElementById("restaurantNameError").style.visibility="visible";      
        valid=false;
    }
    if(address=="")
    {
        document.getElementById("addressError").style.visibility="visible";
        valid=false;
    }
    if(!(telephoneRegex.test(telephone)))
    {
        document.getElementById("telephoneError").style.visibility="visible";
        valid=false;
    }
    if(!(emailRegex.test(email)))
    {
        document.getElementById("emailError").style.visibility="visible";
        valid=false;
    }
    if(password.length<8)
    {
        document.getElementById("passwordError").style.visibility="visible";
        valid=false;
    } 
    if(password!=confirmPassword)
    {
        document.getElementById("confirmPasswordError").style.visibility="visible";
        valid=false;
    }
        return valid;
}

function resetFunction()
{
    document.getElementById("restaurantNameError").style.visibility="hidden";
    document.getElementById("addressError").style.visibility="hidden";
    document.getElementById("telephoneError").style.visibility="hidden";
    document.getElementById("emailError").style.visibility="hidden";
    document.getElementById("passwordError").style.visibility="hidden";
    document.getElementById("confirmPasswordError").style.visibility="hidden";
    return true;
}