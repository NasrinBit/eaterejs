Student Developer : Nasarinbahen Kureshi
Application: Capstone Project - Eater Food Ordering System
Instructions:
To see development code working follow below steps
================================================================
1. download folder EATEREJS from below path
	\CovidFighters\4.0 Development\Draft\Nasrin\EATEREJS 
2. Open EATEREJS folder from below path in Visual Studio Code
	\CovidFighters\4.0 Development\Draft\Nasrin\EATEREJS  
3. Open New Terminal in Visual Studio Code
4.	run below command in Terminal Command Prompt
	node index.js
5. Open webbrowser type below link it will take you to "EATER Food Ordering System"
	localhost:8082/
